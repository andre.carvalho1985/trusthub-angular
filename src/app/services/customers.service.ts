import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
@Injectable({
  providedIn: "root"
})
export class CustomersService {
  private API_URL = "https://whispering-tor-93658.herokuapp.com";
  errorMessage: string[] = [];

  constructor(private httpClient: HttpClient, private router: Router) {}

  getCustomers() {
    return this.httpClient.get(`${this.API_URL}/customers`);
  }

  getCustomerById(id) {
    return this.httpClient.get(`${this.API_URL}/customers/${id}`);
  }

  createCustomer(customer) {
    this.errorMessage.length = 0;
    this.httpClient.post(`${this.API_URL}/customers`, customer).subscribe(
      () => this.router.navigate(["/customers"]),
      err => {
        if (err.error.errors) {
          err.error.errors.map(e => {
            this.errorMessage.push(e.defaultMessage);
          });
        } else {
          this.errorMessage.push("Verifique os dados e tente novamente.");
        }
      }
    );
  }

  editCustomer(customer) {
    this.errorMessage.length = 0;
    this.httpClient
      .put(`${this.API_URL}/customers/${customer.id}`, customer)
      .subscribe(
        () => this.router.navigate(["/customers"]),
        err => this.errorMessage.push("Não foi possível editar.")
      );
  }

  deleteCustomer(id) {
    this.errorMessage.length = 0;
    this.httpClient
      .delete(`${this.API_URL}/customers/${id}`)
      .subscribe(
        () => this.router.navigate(["/customers"]),
        err => this.errorMessage.push("Não foi possível excluir.")
      );
  }

  getErrorMessages() {
    return this.errorMessage;
  }
}

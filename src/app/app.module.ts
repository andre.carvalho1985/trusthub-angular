import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { RouterModule, Routes, Router } from "@angular/router";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { MainComponent } from "./main/main.component";
import { DetailsComponent } from "./details/details.component";
import { CustomersComponent } from "./customers/customers.component";
import { HttpClientModule } from "@angular/common/http";
import { RegisterComponent } from "./register/register.component";
import { FormsModule } from "@angular/forms";

const appRoutes: Routes = [
  { path: "", component: MainComponent },
  { path: "customers", component: CustomersComponent },
  { path: "customers/:id", component: DetailsComponent },
  { path: "registration", component: RegisterComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MainComponent,
    DetailsComponent,
    CustomersComponent,
    RegisterComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

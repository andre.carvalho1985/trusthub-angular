import { Component, OnInit } from "@angular/core";
import { Customer } from "../class/customer";
import { CustomersService } from "../services/customers.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  customer: Customer;

  constructor(private customersService: CustomersService) {
    this.customer = new Customer();
  }

  ngOnInit() {}
  createCustomer() {
    this.customersService.createCustomer(this.customer);
  }

  errorMessages = this.customersService.getErrorMessages();
}

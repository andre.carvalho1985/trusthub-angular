export class Customer {
  name: string;
  creditLimit: number;
  creditRisk: string;
}

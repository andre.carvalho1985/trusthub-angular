import { Component, OnInit } from "@angular/core";
import { CustomersService } from "../services/customers.service";

@Component({
  selector: "app-customers",
  templateUrl: "./customers.component.html",
  styleUrls: ["./customers.component.css"]
})
export class CustomersComponent implements OnInit {
  public customers: Array<object> = [];

  constructor(private customersService: CustomersService) {}

  ngOnInit() {
    this.getCustomers();
  }
  public getCustomers() {
    this.customersService.getCustomers().subscribe((data: Array<object>) => {
      this.customers = data;
    });
  }
}

import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { CustomersService } from "../services/customers.service";

@Component({
  selector: "app-details",
  templateUrl: "./details.component.html",
  styleUrls: ["./details.component.css"]
})
export class DetailsComponent implements OnInit {
  customer: any;
  id: string;
  disabled: boolean;
  constructor(
    private route: ActivatedRoute,
    private customersService: CustomersService
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.params["id"];
    this.disabled = true;
    this.customersService.getCustomerById(this.id).subscribe(data => {
      this.customer = data;
    });
  }
  public edit() {
    this.disabled = !this.disabled;
  }

  editCustomer(customer) {
    this.customersService.editCustomer(customer);
  }

  public deleteCustomer(id) {
    this.customersService.deleteCustomer(id);
  }

  errorMessages = this.customersService.getErrorMessages();
}
